# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2022/10/24 11:08:32 by sada-sil          #+#    #+#              #
#    Updated: 2022/11/22 16:06:43 by sada-sil         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libftprintf.a

LIBFT = libft/libft.a

CC = cc
CFLAGS = -Wall -Werror -Wextra
RM = rm -f

SRCS = 		ft_printf.c \
			src/ft_putchar.c \
			src/ft_putstr.c \
			src/ft_putint.c \
			src/ft_putuint.c \
			src/ft_putnbr_base.c

OBJS = $(SRCS:.c=.o)

${NAME}:	${OBJS}
	$(MAKE) bonus -C ./libft
	cp libft/libft.a $(NAME)
	ar rc ${NAME} ${OBJS}
	ranlib ${NAME}

all:	${NAME}

cmain: ${NAME}
	${CC} ${CFLAGS} main.c ${NAME}

clean:
	${RM} ${OBJS}
	$(MAKE) clean -C ./libft

fclean: clean
	${RM} a.out
	${RM} ${NAME}
	$(MAKE) fclean -C ./libft

re: fclean all

.PHONY: all clean fclean re
