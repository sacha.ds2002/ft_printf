/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/28 16:29:23 by sada-sil          #+#    #+#             */
/*   Updated: 2022/11/03 15:04:46 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_calloc(size_t count, size_t size)
{
	size_t	final_size;
	void	*ptr;

	final_size = count * size;
	if (final_size == 0)
		return ((void *)malloc(0));
	ptr = malloc(final_size);
	if (!ptr)
	{
		free(ptr);
		return ((void *)malloc(0));
	}
	ft_bzero(ptr, final_size);
	return (ptr);
}
