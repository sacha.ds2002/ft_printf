/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putint.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/10 13:56:13 by sada-sil          #+#    #+#             */
/*   Updated: 2022/11/22 12:25:27 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	ft_intlen(int n)
{
	int	i;

	i = 1;
	if (n < 0)
	{
		if (n == -2147483648)
			return (11);
		n *= -1;
		i++;
	}
	while (n >= 10)
	{
		n /= 10;
		i++;
	}	
	return (i);
}

int	ft_putint(int n)
{
	int	len;

	len = ft_intlen(n);
	ft_putnbr_fd(n, 1);
	return (len);
}
